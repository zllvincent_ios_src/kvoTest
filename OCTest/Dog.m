//
//  Dog.m
//  OCTest
//
//  Created by Vincent on 2017/3/15.
//  Copyright © 2017年 Vincent. All rights reserved.
//

#import "Dog.h"

@implementation Dog
{
    int age;
}

-(NSString *)description{

    return [NSString stringWithFormat:@"%@->_name:%@,age:%d",[super description],_name,age];
}

@end
