//
//  main.m
//  OCTest
//
//  Created by Vincent on 2017/3/15.
//  Copyright © 2017年 Vincent. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Person.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
        /*
         使用[obj addObserver:obj2 forKeyPath:key options: context:] 被观察的是obj对象的key属性
         
         使用kvo在对象释放时必须移除观察者，否则程序会崩
            
         */
        
        Dog *dog = [[Dog alloc] init];
        [dog setValue:@"王朝" forKey:@"name"];
        
        
        Person *p = [[Person alloc] initWithDog:dog];
        
        
        p.dog = nil;
        p.dog = [Dog new];
        p.dog.name = @"jack";
       
        
    }
    return 0;
}
