//
//  Person.m
//  OCTest
//
//  Created by Vincent on 2017/3/15.
//  Copyright © 2017年 Vincent. All rights reserved.
//

#import "Person.h"

@implementation Person


-(id)initWithDog:(Dog *)dog{
    self = [super init];
    if(self){
        _dog = dog;
        
        [self addObserver:self forKeyPath:@"dog.name" options:NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld context:NULL];
    }
    return self;
}


-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context{
    
    NSLog(@"object:%@,keyPath:%@,change:%@",NSStringFromClass([object class]),keyPath,change);
}



@end
