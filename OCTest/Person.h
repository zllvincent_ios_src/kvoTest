//
//  Person.h
//  OCTest
//
//  Created by Vincent on 2017/3/15.
//  Copyright © 2017年 Vincent. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Dog.h"

@interface Person : NSObject

@property(nonatomic,strong)Dog *dog;

-(id)initWithDog:(Dog *)dog;

@end
